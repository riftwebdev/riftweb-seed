<?php

namespace Riftweb\Seed;

use Illuminate\Support\ServiceProvider;
use Riftweb\Seed\Commands\SeedCommand;
use Riftweb\Seed\Commands\SeederMakeCommand;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Database\Eloquent\Relations\Relation as Resolver;

class RiftSeedProvider extends ServiceProvider implements DeferrableProvider
{
    public function register()
    {
        $this->app->singleton(SeederMakeCommand::class, function ($app) {
            return new SeederMakeCommand($app['files'], $app['composer']);
        });

        $this->app->singleton(SeedCommand::class, function ($app) {
            return new SeedCommand($app['db']);
        });
    }

    public function boot(): void
    {
        if ($this->app->runningInConsole()) {
            $this->commands([
                SeederMakeCommand::class,
                SeedCommand::class
            ]);

            $this->loadMigrationsFrom(__DIR__.'/../Migrations');

            $this->publishes([
                __DIR__.'/Migrations' => database_path('migrations'),
            ], 'migrations');

            $this->publishes([
                __DIR__.'/Seeders' => database_path('seeders'),
            ], 'seeders');
        }
    }

    public function provides()
    {
        return [
            SeederMakeCommand::class,
            SeedCommand::class
        ];
    }
}
