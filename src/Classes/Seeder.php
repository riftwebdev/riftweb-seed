<?php

namespace Riftweb\Seed\Classes;

use Illuminate\Console\View\Components\TwoColumnDetail;
use Illuminate\Contracts\Container\Container;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Support\Arr;
use Monolog\Handler\IFTTTHandler;
use Riftweb\Seed\Services\DatabaseSeederService;
use Illuminate\Database\Seeder as SeederOriginal;

class Seeder extends SeederOriginal
{
    public function call($class, $silent = false, array $parameters = [])
    {
        $databaseSeederService = app(DatabaseSeederService::class);
        $classes = Arr::wrap($class);

        foreach ($classes as $class) {
            $seeder = $this->resolve($class);

            $name = class_basename($seeder);

            $hasBeenSeeded = $databaseSeederService->hasBeenSeeded($name);

            if ($hasBeenSeeded) {
                static::$called[] = $class;
                continue;
            }

            if ($silent === false && isset($this->command)) {
                with(new TwoColumnDetail($this->command->getOutput()))->render(
                    $name,
                    '<fg=yellow;options=bold>RUNNING</>'
                );
            }

            $startTime = microtime(true);
            $seeder->__invoke($parameters);

            if ($silent === false && isset($this->command)) {
                $runTime = number_format((microtime(true) - $startTime) * 1000);

                with(new TwoColumnDetail($this->command->getOutput()))->render(
                    $name,
                    "<fg=gray>$runTime ms</> <fg=green;options=bold>DONE</>"
                );

                $this->command->getOutput()->writeln('');
            }

            static::$called[] = $class;
        }

        return $this;
    }

    /**
     * Run the database seeds.
     *
     * @param  array  $parameters
     * @return mixed
     *
     * @throws \InvalidArgumentException
     */
    public function __invoke(array $parameters = [])
    {
        if (! method_exists($this, 'run')) {
            throw new InvalidArgumentException('Method [run] missing from '.get_class($this));
        }

        $databaseSeederService = app(DatabaseSeederService::class);

        $className = class_basename($this);
        $hasBeenSeeded = $databaseSeederService->hasBeenSeeded($className);

        if ($hasBeenSeeded) {
            return;
        }

        $callback = fn () => isset($this->container)
            ? $this->container->call([$this, 'run'], $parameters)
            : $this->run(...$parameters);

        $uses = array_flip(class_uses_recursive(static::class));

        if (isset($uses[WithoutModelEvents::class])) {
            $callback = $this->withoutModelEvents($callback);
        }

        $result = $callback();

        $databaseSeederService->store($className);

        return $result;
    }
}
