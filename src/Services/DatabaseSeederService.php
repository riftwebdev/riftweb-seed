<?php

namespace Riftweb\Seed\Services;

use Riftweb\Seed\Models\Seeder;
use Illuminate\Contracts\Console;
use Throwable;

class DatabaseSeederService
{
    public function hasBeenSeeded(string $seeder): bool
    {
        try {
            return Seeder::where('seeder', $seeder)->exists();
        } catch (Throwable $e) {
            report($e);
        }

        return false;
    }

    public function getNextBatch(): int
    {
        try {
            return Seeder::max('batch') + 1;
        } catch (Throwable $e) {
            report($e);
            return 1;
        }
    }

    public function store(string $seeder): void
    {
        try {
            Seeder::create([
                'seeder' => $seeder,
                'batch' => $this->getNextBatch()
            ]);
        } catch (Throwable $e) {
            report($e);
        }
    }
}
