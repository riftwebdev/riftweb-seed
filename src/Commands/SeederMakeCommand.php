<?php

namespace Riftweb\Seed\Commands;

use Illuminate\Database\Console\Seeds\SeederMakeCommand as SeederMakeCommandOriginal;
use Symfony\Component\Console\Attribute\AsCommand;

#[AsCommand(name: 'make:seeder')]
class SeederMakeCommand extends SeederMakeCommandOriginal
{
    public function handle()
    {
        parent::handle();
    }
    protected function getStub()
    {
        return $this->resolveStubPath('/../stubs/seeder.stub');
    }

    protected function resolveStubPath($stub)
    {
        return __DIR__ . $stub;
    }
}