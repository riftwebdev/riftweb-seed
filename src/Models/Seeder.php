<?php

namespace Riftweb\Seed\Models;

use Illuminate\Database\Eloquent\Model;

class Seeder extends Model
{
    protected $fillable = [
        'seeder',
        'batch'
    ];

    protected $casts = [
        'batch' => 'integer'
    ];
}
